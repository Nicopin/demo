import Vue from 'vue';
import Vuetify from 'vuetify/lib';
Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme:{
    themes: {
      light: {
        primary: "#00E5FF", // #E53935
        secondary: "#34495e", // #FFCDD2
        accent: "#2980b9", // #3F51B5
        error: "#c0392b",
        warning: "#f1c40f",
        info: "#9b59b6",
        success: "#27ae60"
      },
    },
  }
});
