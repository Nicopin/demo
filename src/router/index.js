import Vue from 'vue';
import Router from 'vue-router';
import { isAuthenticated } from '../components/utilitys/auth'
import Login from '@/components/login/Login';
import Dashboard from '@/components/dashboard/Dashboard';
import Loader from '@/components/loader/Loader';
import Home from '@/components/home/home';
import Registers from '@/components/registers/registers';
import Data from '@/components/registers/data';
import Users from '@/components/registers/users';
import Account from '@/components/registers/account';
import Diccionario from '@/components/registers/diccionario';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Login,
            children: [
                {
                    path: 'login',
                    redirect: '/'
                }
            ],
            beforeEnter: function (to, from, next) {
                if (isAuthenticated()) {
                    next('/dashboard')
                }
                else {
                    next()
                }
            }
        },
        {
            path: '/dashboard',
            component: Dashboard,
            children: [
                {
                    path: '',
                    redirect: 'home'
                },
                {
                    path: 'loader',
                    component: Loader
                },
                {
                    path: 'home',
                    component: Home
                },
                {
                    path: 'registers',
                    component: Registers
                },
                {
                    path: 'data',
                    component: Data
                },
                {
                    path: 'users',
                    component: Users
                },
                {
                    path: 'account',
                    component: Account
                },
                {
                    path: 'diccionario',
                    component: Diccionario
                }
            ],
            beforeEnter: function (to, from, next) {
                if (!isAuthenticated()) {
                    next('/login')
                }
                else {
                    next()
                }
            }
        },
        {
            path: '**',
            redirect: '/'
        }
    ]
});
